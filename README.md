# DRAPAC DNS Resolver

### What is this?

An encrypted DNS service that uses [DNS over HTTPS](https://en.wikipedia.org/wiki/DNS_over_HTTPS) (DoH) to protect online privacy and security by blocking ads, trackers, and malware on the web.

### How to set it up?

To use the DoH on your desktop computer, look for "secure DNS" in your web broser settings. Set it to `https://dns.drap.ac/dns-query`.

### What is blocked?

Online advertising, tracking, and malicious hostnames compiled in block lists and list sets by:

- [ABPVN](https://github.com/abpvn/abpvn)
- [AdAway](https://github.com/AdAway/adaway.github.io/)
- [AdGuard](https://github.com/AdguardTeam/AdguardSDNSFilter)
- [ABPindo](https://github.com/ABPindo/indonesianadblockrules)
- [List-KR](https://github.com/List-KR/List-KR)
- [Mullvad](https://github.com/mullvad/dns-blocklists#trackers) 

Additionally, this DNS service also uses [DRAPAC General Block List](https://gitlab.com/drapac.net/dns/-/blob/main/block-lists/general.txt).

### What is not blocked?

This DNS service is intended for protecting online privacy and security by blocking ads, trackers, and malware. Although it may also be used to circumvent internet censorship, this is not the service is set up to do.

This DNS service does not block pornography, gambling, or social networking and is not meant for the purpose of parental control.